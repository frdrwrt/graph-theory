package com.example.graph

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

internal class BreadthFirstSearchTest{

    companion object{
        val graph: Graph = Graph(6)

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            graph.addEdge(0, 1)
            graph.addEdge(0, 2)
            graph.addEdge(2, 3)
            graph.addEdge(1, 4)
            graph.addEdge(4, 5)
        }
    }

    @Test
    fun `test queue`() {
        val bfs = BreadthFirstSearch(graph, 0)
        assertIterableEquals(arrayListOf(null, 0, 0, 2, 1, 4), bfs.parents )
    }

    @Test
    fun `test shortest Path`() {
        val bfs = BreadthFirstSearch(graph, 0)
        assertIterableEquals(arrayListOf(0,1,4,5), bfs.shortestPath(0, 5))
        assertIterableEquals(arrayListOf(0,2,3), bfs.shortestPath(0, 3))
        assertIterableEquals(arrayListOf(1,4,5), bfs.shortestPath(1, 5))
        assertNull(bfs.shortestPath(2, 5))

    }

}