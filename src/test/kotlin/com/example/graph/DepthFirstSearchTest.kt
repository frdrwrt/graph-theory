package com.example.graph

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

internal class DepthFirstSearchTest{

    companion object {
        val graph: Graph = Graph(6)

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            graph.addEdge(0, 1)
            graph.addEdge(0, 2)
            graph.addEdge(2, 3)
            graph.addEdge(4, 5)
        }
    }

    @Test
    fun `correct behaviour`() {
        val dfs = DepthFirstSearch(graph, 0)
        println(dfs.visited)
        assertIterableEquals(listOf(true, true, true, true, false, false), dfs.visited)
    }


}