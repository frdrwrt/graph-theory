package com.example.graph

import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

internal class GraphTest {

    val graph = Graph(4)

    @Test
    fun `graph should contain 4 nodes`() {
        println(graph.adjacencyList)
        assertEquals(4, graph.adjacencyList.size)
    }

    @Test
    fun `graph should add edges correct`(){
        graph.addEdge(0, 1)
        graph.addEdge(0, 2)
        graph.addEdge(1, 3)
        graph.addEdge(3, 4)
        assertIterableEquals(mutableListOf<Int>(1,2), graph.adjacencyList[0])
        assertIterableEquals(mutableListOf<Int>(3), graph.adjacencyList[1])
    }
}
