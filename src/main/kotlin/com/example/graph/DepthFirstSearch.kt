package com.example.graph

class DepthFirstSearch(val graph: Graph, val startNode: Int){

    val visited: MutableList<Boolean> = MutableList(graph.numNodes){false}

    init {
        visit(startNode)
    }

    private fun visit(node: Int) {
        if (!visited[node]) {
            visited[node] = true
            for (neighbour in graph.adjacencyList[node]) visit(neighbour)
        }
    }
}
