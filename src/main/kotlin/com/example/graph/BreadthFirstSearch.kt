package com.example.graph

class BreadthFirstSearch(val graph: Graph, startNode: Int) {

    private val visited: MutableList<Boolean> = MutableList(graph.numNodes) { false }
    val parents: MutableList<Int?> = MutableList(graph.numNodes) { null }
    private val queue: MutableSet<Int> = mutableSetOf()

    init {
        queue.add(startNode)
        while (queue.isNotEmpty()) {
            val node = queue.first()
            visit(node)
            queue.remove(node)
        }

    }

    private fun visit(node: Int) {
        for (neighbour in graph.adjacencyList[node]) {
            if (!visited[neighbour]) {
                queue.add(neighbour)
                visited[neighbour] = true
                parents[neighbour] = node
            }
        }
    }

    fun shortestPath(start: Int, end: Int): List<Int>? {
        val path: MutableSet<Int> = mutableSetOf()
        var node: Int? = end
        while (node != null) {
            path.add(node)
            if (node == start) break
            node = parents[node]
        }
        return if (path.contains(start)) {
            path.reversed()
        } else {
            null
        }

    }
}