package com.example.graph

class Graph(val numNodes: Int) {

    val adjacencyList: MutableList<MutableList<Int>> = mutableListOf()

    init {
        for (num in 0..numNodes-1) adjacencyList.add(mutableListOf())
    }

    fun addEdge(from: Int, to: Int) {
        adjacencyList[from].add(to)
    }

}
